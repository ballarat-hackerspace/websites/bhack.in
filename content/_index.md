---
description: "Ballarat Hackerspace Linktree!"
---

If you're a member with an [oasis](https://oasis.ballarathackerspace.org.au) account looking
for your homepage it is at https://bhack.in/~nobody (replace _nobody_ with your
[oasis](https://oasis.ballarathackerspace.org.au) username)

<div class="events">
<h3>:calendar: Upcoming events</h3>

<ul id="events"></ul>
</div>
