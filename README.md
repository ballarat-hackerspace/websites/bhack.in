# bhack.in

## Local Development

Ensure you have `hugo` and `go` installed, checkout the repo and then run the following:

```
$ cd bhack.in
$ git submodule update --init --recursive
$ hugo server
```

Now browse to [localhost:1313](http://localhost:1313)

Make any changes, e.g. adding/removing/updating links in the `config.toml` page.

## Deployment

You will need to build the site via `hugo` then copy the resulting contents of the
`public/` directory onto `oasis:/opt/www/bhack.in/html`
